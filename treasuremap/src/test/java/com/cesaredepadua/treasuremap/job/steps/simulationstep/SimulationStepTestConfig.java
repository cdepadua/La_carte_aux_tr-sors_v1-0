package com.cesaredepadua.treasuremap.job.steps.simulationstep;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cesaredepadua.treasuremap.job.buisinessclasses.SimulationMap;
import com.cesaredepadua.treasuremap.job.buisinessclasses.Simulation;
import com.cesaredepadua.treasuremap.job.services.SimulationService;
import com.cesaredepadua.treasuremap.job.steps.simulationstep.processor.SimulationStepProcessor;
import com.cesaredepadua.treasuremap.job.steps.simulationstep.writer.SimulationStepWriter;

/**
 * @author cesar
 * Configuration for tests
 */
@Configuration
public class SimulationStepTestConfig {
	
	/** Output file path. */
	@Value("${batch.output.file.path}")
	private String outputFilePath;
	/** Output file name. */
	@Value("${batch.output.file.name}")
	private String outputFileName;
	/** Line separator output. */
	private String outputLineSeparator = "\n";
	
	/**
	 * @return processor
	 */
	@Bean
	SimulationStepProcessor processor()
	{
		return new SimulationStepProcessor();
	}
	
	/**
	 * @return simulation service
	 */
	@Bean
	SimulationService simulationService()
	{
		return new SimulationService();
	}
	
	/**
	 * @return writer
	 */
	@Bean
	SimulationStepWriter writer()
	{
		String file = new StringBuilder().append(outputFilePath).append(outputFileName).toString();
		return new SimulationStepWriter(file, this.outputLineSeparator);
	}
	
	/**
	 * @return simulation
	 */
	@Bean
	Simulation simulation()
	{
		return new Simulation();
	}
	
	/**
	 * @return map
	 */
	@Bean
	SimulationMap map()
	{
		return new SimulationMap();
	}
}

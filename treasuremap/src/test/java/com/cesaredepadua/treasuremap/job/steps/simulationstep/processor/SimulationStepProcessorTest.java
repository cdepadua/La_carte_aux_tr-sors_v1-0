package com.cesaredepadua.treasuremap.job.steps.simulationstep.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cesaredepadua.treasuremap.job.steps.simulationstep.SimulationStepTestConfig;
import com.cesaredepadua.treasuremap.job.steps.simulationstep.processor.SimulationStepProcessor;

import org.junit.Assert;

/**
 * @author cesar
 * Tests for processor
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringJUnitConfig(classes={SimulationStepTestConfig.class})
public class SimulationStepProcessorTest {
	
	/**
	 * Input data
	 */
	private List<Map<String, String>> input;
	
	/**
	 * Processor
	 */
	@Autowired
	SimulationStepProcessor process;
	
	/**
	 * Generate a map with only plain
	 */
	private void generateOnlyPlainMockedData()
	{
		List<Map<String, String>> list = new ArrayList<>();
		Map<String,String> map = new HashMap<>();
		map.put("width", "3");
		map.put("key", "C");
		map.put("height", "4");
		list.add(map);
		this.input = list;
	}
	
	/**
	 * Generate a mocked map
	 */
	private void generateAMapMockedData()
	{
		List<Map<String, String>> list = new ArrayList<>();
		
		Map<String,String> t = new HashMap<>();
		t.put("width", "3");
		t.put("key", "C");
		t.put("height", "4");
		list.add(t);
		
		t.put("xPos", "1");
		t.put("key", "M");
		t.put("yPos", "0");
		list.add(t);
		
		t.put("xPos", "2");
		t.put("key", "M");
		t.put("yPos", "1");
		list.add(t);
		
		t.put("xPos", "0");
		t.put("key", "T");
		t.put("yPos", "3");
		t.put("nbOfTreasures", "2");
		
		t.put("xPos", "1");
		t.put("key", "T");
		t.put("yPos", "3");
		t.put("nbOfTreasures", "3");
		
		t.put("xPos", "1");
		t.put("key", "A");
		t.put("yPos", "1");
		t.put("name", "Lara");
		t.put("orientation", "S");
		t.put("movements", "AADADAGGA");
		list.add(t);
		
		this.input = list;
		
	}
	
	/**
	 * Test result with only plains
	 */
	@Test
	public void testProcessorGenerateOnlyPlaine()
	{
		generateOnlyPlainMockedData();
		List<String> result = null;
		try {
			result = this.process.process(this.input);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertNotNull("Result must not be null", result);
		Assert.assertEquals("This result must contain only one line",1, result.size());
		Assert.assertEquals("C${batch.output.file.separator}3${batch.output.file.separator}4",result.get(0));
	}
	
	/**
	 * Test processor with mocked data
	 */
	@Test
	public void testProcessor()
	{
		generateAMapMockedData();
		List<String> result = null;
		try {
			result = this.process.process(this.input);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertNotNull("Result must not be null", result);
		Assert.assertEquals("This result must contain only 5 line",5, result.size());
		Assert.assertEquals(result.get(0), "C${batch.output.file.separator}3${batch.output.file.separator}4");
		Assert.assertEquals(result.get(4), "A${batch.output.file.separator}Lara${batch.output.file.separator}1${batch.output.file.separator}1${batch.output.file.separator}S${batch.output.file.separator}0");
	}
}

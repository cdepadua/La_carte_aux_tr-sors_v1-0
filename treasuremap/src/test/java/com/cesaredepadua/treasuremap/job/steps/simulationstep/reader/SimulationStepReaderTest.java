package com.cesaredepadua.treasuremap.job.steps.simulationstep.reader;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cesaredepadua.treasuremap.job.steps.simulationstep.reader.SimulationStepReader;

/**
 * @author cesar
 * Tests for reader
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(locations="classpath:application-test.properties")
public class SimulationStepReaderTest {
	/** Input file path. */
	@Value("${batch.input.file.path}")
	private String inputFilePath;
	/** Input file name. */
	@Value("${batch.input.file.name}")
	private String inputFileName;
	/** Field separator */
	@Value("${batch.input.file.separator}")
	private String separator;
	
	/**
	 * Test the reader
	 * @throws Exception exception
	 */
	@Test
	public void testReader() throws Exception
	{
		String file = new StringBuilder().append(inputFilePath).append(inputFileName).toString();
		SimulationStepReader delegate = new SimulationStepReader(file, separator);
		List<Map<String,String>> result = null;
		try
		{
			delegate.open(new ExecutionContext());
			try
			{
				result = delegate.read();
			}
			finally
			{
				delegate.close();
			}
		}
		catch (Exception e)
		{
			Assert.fail();
		}
		Assert.assertNotNull(result);
		Assert.assertEquals("This element must contain 6 line", 6, result.size());
		Assert.assertEquals("The 1st line must contain 3 fields", 3, result.get(0).keySet().size());
		Assert.assertEquals("The 2nd line must contain 3 fields", 3, result.get(1).keySet().size());
		Assert.assertEquals("The 3th line must contain 4 fields", 4, result.get(3).keySet().size());
		Assert.assertEquals("The 6th line must contain 6 fields", 6, result.get(5).keySet().size());
	}
}

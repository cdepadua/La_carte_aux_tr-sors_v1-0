package com.cesaredepadua.treasuremap.job.steps.simulationstep.writer;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cesaredepadua.treasuremap.job.steps.simulationstep.SimulationStepTestConfig;
import com.cesaredepadua.treasuremap.job.steps.simulationstep.writer.SimulationStepWriter;


/**
 * @author cesar
 * Tests for writer
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(locations="classpath:application-test.properties")
@SpringJUnitConfig(classes={SimulationStepTestConfig.class})
public class SimulationStepWriterTest {
	
	/**
	 * Writer
	 */
	@Autowired
	private SimulationStepWriter writer;
	/** Output file path. */
	@Value("${batch.output.file.path}")
	private String outputFilePath;
	/** Output file name. */
	@Value("${batch.output.file.name}")
	private String outputFileName;
	/** Comparator. */
	@Value("${batch.output.file.comparator}")
	private String comparator;

	/**
	 * Test the writer with mocked data
	 */
	@Test 
	public void writerTest()
	{
		List<List<String>> input = new ArrayList<>();
		List<String> listOfValues = Arrays.asList("C-3-4", "M-1-0", "M-2-1", "T-1-3-2", "A-Lara-0-3-S-3");
		input.add(listOfValues);
		try
		{
			this.writer.open(new ExecutionContext());
			try
			{
				this.writer.write(input);
			}
			finally
			{
				this.writer.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Assert.fail();
		}
		String path = new StringBuilder().append(outputFilePath).append(outputFileName).toString();
		File file = new File(path);
		File expected = new File(comparator);
		assertTrue(file.exists());
		try {
			Assert.assertEquals("Files must have same content",FileUtils.readFileToString(expected, "utf-8"), 
				    FileUtils.readFileToString(file, "utf-8"));
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
}

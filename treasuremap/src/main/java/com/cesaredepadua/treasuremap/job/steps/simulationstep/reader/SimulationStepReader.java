package com.cesaredepadua.treasuremap.job.steps.simulationstep.reader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.core.io.FileSystemResource;

import com.cesaredepadua.treasuremap.job.enumclasses.EntryKeys;
import com.cesaredepadua.treasuremap.job.mappers.ListFieldSetMapper;

/**
 * @author cesar
 * Custom reader for simulation step
 */
public class SimulationStepReader  implements ItemReader<List<Map<String, String>>>, ItemStream{
	
	/**
	 * Logger
	 */
	@SuppressWarnings("unused")
	private static final Log LOG = LogFactory.getLog(SimulationStepReader.class);
	
	/**
	 * Default reader for flat files
	 */
	private FlatFileItemReader<Map<String, String>> delegate;
	
	/**
	 * Return the reader
	 * @param file input file
	 * @param separator field separator
	 */
	public SimulationStepReader(final String file, final String separator) {
		Map<String, LineTokenizer> tokenizers = new HashMap<>();
		tokenizers.put(EntryKeys.MAP_SIZE.key(), new DelimitedLineTokenizer(separator) {
            {
            	this.setNames(EntryKeys.MAP_SIZE.valuesHeaders());
            }
		});
		tokenizers.put(EntryKeys.MOUNTAIN.key(), new DelimitedLineTokenizer(separator) {
            {
            	this.setNames(EntryKeys.MOUNTAIN.valuesHeaders());
            }
		});
		tokenizers.put(EntryKeys.TRASURE.key(), new DelimitedLineTokenizer(separator) {
            {
            	this.setNames(EntryKeys.TRASURE.valuesHeaders());
            }
		});
		tokenizers.put(EntryKeys.ADVENTURER.key(), new DelimitedLineTokenizer(separator) {
            {
            	this.setNames(EntryKeys.ADVENTURER.valuesHeaders());
            }
		});
		
		Map<String, FieldSetMapper<Map<String, String>>> fieldSetMapper = new HashMap<>();
		fieldSetMapper.put("*", new ListFieldSetMapper());
		
		FlatFileItemReader<Map<String, String>> fileReader = new FlatFileItemReader<Map<String, String>>();
		fileReader.setName("listItemReader");
		fileReader.setResource(new FileSystemResource(file));
		fileReader.setLineMapper(new PatternMatchingCompositeLineMapper<Map<String, String>>() {
	        {
	            this.setTokenizers(tokenizers);
	            this.setFieldSetMappers(fieldSetMapper);
	        }
	    });
		this.delegate = fileReader;
	}

	@Override
	public List<Map<String, String>> read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		List<Map<String, String>> results = new ArrayList<>();
		Boolean end = true;
		
		for(Map<String,String> line = null; (line = this.delegate.read()) != null;)
		{
			end = false;
			results.add(line);
		}
		
		if(end)
			results = null;
		return results;
	}

	/**
	 * Set the reader
	 * @param delegate reader
	 */
	public void setDelegate(FlatFileItemReader<Map<String, String>> delegate) {
		this.delegate = delegate;
	}
	
	@Override
	public void close() throws ItemStreamException {
		this.delegate.close();
	}
	@Override
	public void open(ExecutionContext executionContext) throws ItemStreamException {
		this.delegate.open(executionContext);
	}
	@Override
	public void update(ExecutionContext executionContext) throws ItemStreamException {
		this.delegate.update(executionContext);
	}

}

package com.cesaredepadua.treasuremap.job.steps.simulationstep;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cesaredepadua.treasuremap.job.steps.simulationstep.processor.SimulationStepProcessor;
import com.cesaredepadua.treasuremap.job.steps.simulationstep.reader.SimulationStepReader;
import com.cesaredepadua.treasuremap.job.steps.simulationstep.writer.SimulationStepWriter;

/**
 * @author cesar
 * Simulation main step
 */
@Configuration
public class SimulationStep {
	
	/** Input file path. */
	@Value("${batch.input.file.path}")
	private String inputFilePath;
	/** Input file name. */
	@Value("${batch.input.file.name}")
	private String inputFileName;
	/** Field separator */
	@Value("${batch.input.file.separator}")
	private String separator;
	/** Output file path. */
	@Value("${batch.output.file.path}")
	private String outputFilePath;
	/** Output file name. */
	@Value("${batch.output.file.name}")
	private String outputFileName;
	/** Line separator output. */
	private String outputLineSeparator = "\n";
	
	
	/**
	 * @return reader
	 */
	@Bean
	public SimulationStepReader reader()
	{
		String file = new StringBuilder().append(inputFilePath).append(inputFileName).toString();
		return new SimulationStepReader(file, this.separator);
	}
	
	/**
	 * @return processor
	 */
	@Bean
	public SimulationStepProcessor processor()
	{
		return new SimulationStepProcessor();
	}
	
	/**
	 * @return writer
	 */
	@Bean
	public SimulationStepWriter writer() 
	{
		String file = new StringBuilder().append(outputFilePath).append(outputFileName).toString();
		return new SimulationStepWriter(file, this.outputLineSeparator);
	}
}

package com.cesaredepadua.treasuremap.job;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cesaredepadua.treasuremap.job.steps.simulationstep.SimulationStep;


/**
 * @author cesar
 *Configuration file for job
 */
@Configuration
@EnableBatchProcessing
public class ProcessingMapJobConfig extends DefaultBatchConfigurer{
	
	/**
	 * JobBuilderFactory
	 */
	@Autowired
	public JobBuilderFactory jobBuilderFactory;
	/**
	 * StepBuilderFactory
	 */
	@Autowired
	public StepBuilderFactory stepBuilderFactory;
	
	@Override
    public void setDataSource(DataSource dataSource) {
        //This BatchConfigurer ignores any DataSource
    }
	

	
	/**
	 * Main Job.
	 * @param simulation simulation step
	 * @return the job
	 */
	@Bean
	public Job processingMapJob(Step simulation)
	{
		return jobBuilderFactory.get("processingMapJob")
				.incrementer(new RunIdIncrementer())
				.flow(simulation)
				.end()
				.build();
	}
	
	/**
	 * Simulaition step
	 * @param step simulation step
	 * @return simulation step
	 */
	@Bean
	public Step simulation(SimulationStep step)
	{
		return stepBuilderFactory.get("parseFile")
				.<List<Map<String, String>>,List<String>> chunk(100)
				.reader(step.reader())
				.processor(step.processor())
				.writer(step.writer())
				.build();
	}
}

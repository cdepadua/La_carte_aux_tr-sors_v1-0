package com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.impl;

import com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.TileInterface;
import com.cesaredepadua.treasuremap.job.enumclasses.TilesType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author cesar
 * Abstract tile
 */
public abstract class AbstractTile implements TileInterface{
	/** x position. */
	@Getter @Setter
	protected Integer x;
	/** y position. */
	@Getter @Setter
	protected Integer y;
	/** Tile type. */
	@Getter @Setter
	protected TilesType type;
}

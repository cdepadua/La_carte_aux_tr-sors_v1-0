package com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.impl;

import com.cesaredepadua.treasuremap.job.enumclasses.TilesType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author cesar
 * Field business class
 */
public class Field extends AbstractTile{
	/** Traversable. */
	@Getter @Setter
	protected Boolean traversable;
	/** Occupied. */
	@Getter @Setter
	protected Boolean occupied;
	
	/** Constructor. 
	 * @param t tile type */
	public Field(final TilesType t)
	{
		this.type = t;
		this.occupied = false;
		if(TilesType.PLAIN.equals(t))
			this.traversable = true;
		else if(TilesType.MOUNTAIN.equals(t))
			this.traversable = false;
		else if(TilesType.TREASURE.equals(t))
			this.traversable = true;
	}
	
	@Override
	public String toString()
	{
		return this.type.symbol();
	}
}

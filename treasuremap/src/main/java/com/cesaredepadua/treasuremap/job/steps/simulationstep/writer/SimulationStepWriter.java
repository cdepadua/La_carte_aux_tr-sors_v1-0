package com.cesaredepadua.treasuremap.job.steps.simulationstep.writer;

import java.util.List;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.ResourceAwareItemWriterItemStream;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;


/**
 * @author cesar
 * Custom writer for simulation step
 */
public class SimulationStepWriter implements ResourceAwareItemWriterItemStream<List<String>>{

	/**
	 * Default writer
	 */
	private FlatFileItemWriter<List<String>> delegate;
	
	/**
	 * Constructor
	 * @param file output file
	 * @param outputLineSeparator line separator
	 */
	public SimulationStepWriter(final String file, String outputLineSeparator) {
		this.delegate = new FlatFileItemWriter<>();
		DelimitedLineAggregator<List<String>> lineAggregator = new DelimitedLineAggregator<List<String>>();
		lineAggregator.setDelimiter(outputLineSeparator);
		this.delegate.setLineAggregator(lineAggregator);
		this.delegate.setResource(new FileSystemResource(file));
		this.delegate.setName("listItemWriter");
	}

	@Override
	public void open(ExecutionContext executionContext) throws ItemStreamException {
		this.delegate.open(executionContext);
	}

	@Override
	public void update(ExecutionContext executionContext) throws ItemStreamException {
		this.delegate.update(executionContext);
	}

	@Override
	public void close() throws ItemStreamException {
		this.delegate.close();
	}

	@Override
	public void setResource(Resource resource) {
		this.delegate.setResource(resource);
	}

	@Override
	public void write(List<? extends List<String>> items) throws Exception {
		items.forEach(i -> {
			try {
				this.delegate.write(items);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
	

}

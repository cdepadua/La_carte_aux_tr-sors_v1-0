package com.cesaredepadua.treasuremap.job.enumclasses;

/**
 * @author cesar
 * Enum of tyle tipes
 */
public enum TilesType {
	/**
	 * Map size
	 */
	MAP_SIZE("C"),
	/**
	 * Plain
	 */
	PLAIN("."),
	/**
	 * Mountain
	 */
	MOUNTAIN("M"),
	/**
	 * Treasure
	 */
	TREASURE("T"),
	/**
	 * Adventurer
	 */
	ADVENTURER("A");
	
	/**
	 * Symbol
	 */
	private final String symbol;
	
	/** Constructor. 
	 * @param symbol symbol*/
	TilesType(String symbol)
	{
		this.symbol = symbol;
	}
	/**
	 * Getter for symbol.
	 * @return symbol
	 */
	public String symbol()
	{
		return this.symbol;
	}
}

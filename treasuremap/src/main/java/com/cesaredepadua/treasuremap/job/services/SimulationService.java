package com.cesaredepadua.treasuremap.job.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cesaredepadua.treasuremap.job.buisinessclasses.Simulation;
import com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.TileInterface;
import com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.impl.Adventurer;
import com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.impl.Field;
import com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.impl.Treasure;
import com.cesaredepadua.treasuremap.job.enumclasses.Move;
import com.cesaredepadua.treasuremap.job.enumclasses.Orientation;
import com.cesaredepadua.treasuremap.job.enumclasses.TilesType;

/**
 * @author cesar
 * Simulation service
 */
@Service
public class SimulationService {
	
	/**
	 * Logger
	 */
	private static Logger LOG = LoggerFactory.getLogger(SimulationService.class);
	
	/**
	 * Simulation
	 */
	@Autowired
	private Simulation simulation;
	
	/**
	 * Create a map with only plains
	 * @param h height
	 * @param w width
	 * @return map tiles
	 */
	private TileInterface[][] createEmptyMap(Integer h, Integer w)
	{
		TileInterface[][] tiles = new Field[h][w];
		for(Integer i = 0; i < h; i++)
		{
			for (Integer j = 0; j < w; j++)
			{
				tiles[i][j] = new Field(TilesType.PLAIN);
				((Field) tiles[i][j]).setY(j);
				((Field) tiles[i][j]).setX(i);
			}
		}
		return tiles;
	}
	
	/**
	 * Initialize the tile array
	 * @param simulation simulation
	 * @param value line
	 */
	private void initTileArray(Simulation simulation, Map<String, String> value)
	{
		try
		{
			Integer h = Integer.parseInt(value.get("height"));
			Integer w = Integer.parseInt(value.get("width"));
			simulation.getMap().setHeight(h);
			simulation.getMap().setWidth(w);
			simulation.getMap().setTiles((Field[][]) createEmptyMap(h,w));
		}
		catch(NumberFormatException error)
		{
			LOG.info("Map can't be inizialized cause this value is incorrect:" + value);
			throw error;
		}
	}
	
	/**
	 * Add a tile to map
	 * @param simulation simulation
	 * @param line line
	 */
	private void addTileToMap(Simulation simulation, Map<String, String> line)
	{
		TileInterface[][] tiles = simulation.getMap().getTiles();
		try
		{
			if(TilesType.MOUNTAIN.symbol().equals(line.get("key")))
			{
				((Field) tiles[Integer.parseInt(line.get("yPos"))][Integer.parseInt(line.get("xPos"))]).setType(TilesType.MOUNTAIN);
				((Field) tiles[Integer.parseInt(line.get("yPos"))][Integer.parseInt(line.get("xPos"))]).setY(Integer.parseInt(line.get("yPos")));
				((Field) tiles[Integer.parseInt(line.get("yPos"))][Integer.parseInt(line.get("xPos"))]).setX(Integer.parseInt(line.get("xPos")));
				((Field) tiles[Integer.parseInt(line.get("yPos"))][Integer.parseInt(line.get("xPos"))]).setTraversable(false);
				((Field) tiles[Integer.parseInt(line.get("yPos"))][Integer.parseInt(line.get("xPos"))]).setOccupied(false);
			}
			else if(TilesType.TREASURE.symbol().equals(line.get("key")))
			{
				Treasure treasure = new Treasure(TilesType.TREASURE);
				treasure.setNbOftreasures(Integer.parseInt(line.get("nbOfTreasures")));
				treasure.setY(Integer.parseInt(line.get("yPos")));
				treasure.setX(Integer.parseInt(line.get("xPos")));
				tiles[Integer.parseInt(line.get("yPos"))][Integer.parseInt(line.get("xPos"))] = treasure;
			}
		}
		catch(ArrayIndexOutOfBoundsException error)
		{
			LOG.info("This element can't be added cause the coordinate are incorrect: " + line);
		}
		catch(NumberFormatException error)
		{
			LOG.info("This element can't be added cause the coordinate are incorrect: " + line);
		}
	}
	
	/**
	 * Initialize the list of movements
	 * @param strAction string containing the list of actions
	 * @return list of action to list
	 */
	private List<Move> initActionList(String strAction)
	{
		List<Move> move = new ArrayList<>();
		char[] actions= strAction.toCharArray();
		for(Integer i = 0; i < actions.length; i++)
		{
			move.add(Move.move(actions[i]));
		}
		return move;
	}
	
	/**
	 * Add an adventurer to list
	 * @param simulation simulation
	 * @param a adventurer
	 */
	private void addToAdventurersList(Simulation simulation, Map<String,String> a)
	{
		try
		{
			Adventurer adv = new Adventurer();
			adv.setType(TilesType.ADVENTURER);
			adv.setName(a.get("name"));
			adv.setX(Integer.parseInt(a.get("xPos")));
			adv.setY(Integer.parseInt(a.get("yPos")));
			adv.setOrientation(Orientation.orientation(a.get("orientation")));
			adv.setMovements(initActionList(a.get("movements")));
			adv.setNbOfTreasures(0);
			simulation.getAdventurers().add(adv);
		}
		catch(NumberFormatException error)
		{
			LOG.info("This element can't be added cause the coordinate are incorrect: " + a);
		}
	}
	
	/**
	 * Initialize the simulation
	 * @param values lines from file
	 * @return simulation
	 * @throws Exception exception
	 */
	public Simulation initSimulation(final List<Map<String, String>> values) throws Exception
	{
		simulation.setAdventurers(new ArrayList<>());
		
		values.forEach(v ->{
			if(TilesType.MAP_SIZE.symbol().equals(v.get("key")))
			{
				this.initTileArray(simulation, v);
			}
			else if(TilesType.MOUNTAIN.symbol().equals(v.get("key")) || TilesType.TREASURE.symbol().equals(v.get("key")))
			{
				this.addTileToMap(simulation, v);
			}
			else
			{
				addToAdventurersList(simulation, v);
			}
		});
		
		return simulation;
	}
	
	/**
	 * Run the simulation
	 * @param simulation simulation
	 */
	public void runSimulation(Simulation simulation)
	{	Boolean running = true;
		while(running)
		{
			running = false;
			for(Adventurer adventurer : simulation.getAdventurers())
			{
				if(adventurer.moveStepByStep(simulation.getMap()))
					running = true;
			}
		}
	}
	
}

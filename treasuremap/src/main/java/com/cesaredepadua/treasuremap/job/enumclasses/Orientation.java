package com.cesaredepadua.treasuremap.job.enumclasses;

/**
 * @author cesar
 * Enum of orientations
 */
public enum Orientation {
	/**
	 * Nord
	 */
	NORD("N"),
	/**
	 * Sud
	 */
	SUD("S"),
	/**
	 * East
	 */
	EAST("E"),
	/**
	 * West
	 */
	WEST("O");
	
	/**
	 * Symbol
	 */
	private String symbol;
	
	/**
	 * @param s symbol
	 */
	Orientation(String s) {
		this.symbol = s;
	}
	
	/**
	 * @return symbol
	 */
	public String symbol()
	{
		return this.symbol;
	}
	
	/**
	 * @param o orientation letter
	 * @return orientation enum
	 */
	public static Orientation orientation(String o)
	{
		switch(o)
		{
			case "N": return Orientation.NORD;
			case "S": return Orientation.SUD;
			case "E": return Orientation.EAST;
			case "W": return Orientation.WEST;
			default:  return null;
		}
	}
}

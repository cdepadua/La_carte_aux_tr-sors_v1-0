package com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.impl;

import java.util.List;

import com.cesaredepadua.treasuremap.job.buisinessclasses.SimulationMap;
import com.cesaredepadua.treasuremap.job.enumclasses.Move;
import com.cesaredepadua.treasuremap.job.enumclasses.Orientation;
import com.cesaredepadua.treasuremap.job.enumclasses.TilesType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author cesar
 * Adventurer business class
 */
public class Adventurer extends AbstractTile{
	/** Name. */
	@Getter @Setter
	private String name;
	/** Orientation. */
	@Getter @Setter
	private Orientation orientation;
	/** Movements. */
	@Getter @Setter
	private List<Move> movements;
	/** Tresures found. */
	@Getter @Setter
	private Integer nbOfTreasures;
	
	/**
	 * Private method to verify if adventurer is on a treasure
	 * @param map map of simulation
	 */
	private void verifyTreasure(SimulationMap map)
	{
		if(map.getTiles()[this.y][this.x].getType().equals(TilesType.TREASURE))
		{
			if(((Treasure)map.getTiles()[this.y][this.x]).getNbOftreasures() > 0)
			{
				((Treasure)map.getTiles()[this.y][this.x])
				.setNbOftreasures(((Treasure)map.getTiles()[this.y][this.x]).getNbOftreasures() - 1);
				this.nbOfTreasures++;
			}
		}
	}
	
	/**
	 * Private method to move adventurer to sud
	 * @param map map
	 */
	private void validateMovementToSud(SimulationMap map)
	{
		if((this.y + 1) < map.getHeight() && 
		   !map.getTiles()[this.y + 1][this.x].getOccupied() &&
		   map.getTiles()[this.y + 1][this.x].getTraversable())
		{
			map.getTiles()[this.y][this.x].setOccupied(false);
			this.y ++;
			map.getTiles()[this.y][this.x].setOccupied(true);
			verifyTreasure(map);
		}
	}
	
	/**
	 * Private method to move adventurer to nord
	 * @param map map
	 */
	private void validateMovementToNord(SimulationMap map)
	{
		if((this.y - 1) >= 0 && 
			!map.getTiles()[this.y - 1][this.x].getOccupied() &&
			map.getTiles()[this.y - 1][this.x].getTraversable())
		{
			map.getTiles()[this.y][this.x].setOccupied(false);
			this.y--;
			map.getTiles()[this.y][this.x].setOccupied(true);
			verifyTreasure(map);
		}
	}
	
	/**
	 * Private method to move adventurer to east
	 * @param map map
	 */
	private void validateMovementToEast(SimulationMap map)
	{
		if((this.x + 1) < map.getWidth() && 
			!map.getTiles()[this.y][this.x + 1].getOccupied() &&
			map.getTiles()[this.y][this.x + 1].getTraversable())
		{
			map.getTiles()[this.y][this.x].setOccupied(false);
			this.x++;
			map.getTiles()[this.y][this.x].setOccupied(true);
			verifyTreasure(map);
		}
	}
	
	/**
	 * Private method to move adventurer to west
	 * @param map map
	 */
	private void validateMovementToWest(SimulationMap map)
	{
		if((this.x - 1) <= 0 && 
			!map.getTiles()[this.y][this.x - 1].getOccupied() &&
			map.getTiles()[this.y][this.x - 1].getTraversable())
		{
			map.getTiles()[this.y][this.x].setOccupied(false);
			this.x--;
			map.getTiles()[this.y][this.x].setOccupied(true);
			verifyTreasure(map);
		}
	}
	
	/**
	 * Turn adventurer to left
	 */
	private void turnToLeft()
	{
		if(this.orientation.equals(Orientation.NORD))
			this.orientation = Orientation.WEST;
		else if(this.orientation.equals(Orientation.SUD))
			this.orientation = Orientation.EAST;
		else if(this.orientation.equals(Orientation.EAST))
			this.orientation = Orientation.NORD;
		else if(this.orientation.equals(Orientation.WEST))
			this.orientation = Orientation.SUD;
	}
	
	/**
	 * Turn adventurer to right
	 */
	private void turnToRight()
	{
		if(this.orientation.equals(Orientation.NORD))
			this.orientation = Orientation.EAST;
		else if(this.orientation.equals(Orientation.SUD))
			this.orientation = Orientation.WEST;
		else if(this.orientation.equals(Orientation.EAST))
			this.orientation = Orientation.SUD;
		else if(this.orientation.equals(Orientation.WEST))
			this.orientation = Orientation.NORD;
	}
	
	/**
	 * Move adventurer
	 * @param map map
	 * @return adventurer moved
	 */
	public Boolean moveStepByStep(SimulationMap map)
	{
		if(!movements.isEmpty())
		{
			switch (movements.get(0)) {
				case ADVANCE:
					switch (this.orientation) {
						case SUD:
							this.validateMovementToSud(map);
							break;
						case NORD:
							this.validateMovementToNord(map);
							break;
						case WEST:
							this.validateMovementToWest(map);
							break;
						case EAST:
							this.validateMovementToEast(map);
							break;
						default:
							break;
					}
					break;
				case LEFT:
					this.turnToLeft();
					break;
				case RIGHT:
					this.turnToRight();
					break;
				default:
					break;
			}
			this.movements.remove(0);
			return true;
		}
		return false;
	}
	
	@Override
	public String toString()
	{
		return new StringBuilder().append(this.type.symbol()).append("-").append(this.name)
				.append("-").append(x).append("-").append(y).append("-")
				.append(orientation).append("-").append(movements).append("-").append(nbOfTreasures).toString();
	}
}

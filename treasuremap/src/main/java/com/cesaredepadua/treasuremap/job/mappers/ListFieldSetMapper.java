package com.cesaredepadua.treasuremap.job.mappers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.boot.context.properties.bind.BindException;

/**
 * @author cesar
 * Mapper for line
 */
public class ListFieldSetMapper implements FieldSetMapper<Map<String, String>>{
	@Override
    public Map<String, String> mapFieldSet(FieldSet fieldSet) throws BindException {
		Map<String, String> result = new HashMap<String, String>();
		fieldSet.getProperties().forEach((key, value) -> {
			result.put(key.toString(), value.toString().replaceAll("[^A-Za-z0-9]",""));
		});
        return result;
    }
}

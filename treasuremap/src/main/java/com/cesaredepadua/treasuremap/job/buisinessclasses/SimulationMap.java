package com.cesaredepadua.treasuremap.job.buisinessclasses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.impl.Field;

import lombok.Getter;
import lombok.Setter;

/**
 * Class representing a map.
 * @author Cesare
 */
@Component
public class SimulationMap {
	/** Logger. */
	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(SimulationMap.class);
	
	/** Map width. */
	@Getter @Setter
	private Integer width;
	/** Map height. */
	@Getter @Setter
	private Integer height;
	/** Map tiles. */
	@Getter @Setter
	private Field[][] tiles;
	
	/** Read and print the map on console. */
	public void readMapInConsole()
	{
		for(Integer i = 0; i < height; i++)
		{
			for (Integer j = 0; j < width; j++)
			{
				System.out.print("		" + tiles[i][j].toString());
			}
			System.out.print("\n");
		}
	}
}

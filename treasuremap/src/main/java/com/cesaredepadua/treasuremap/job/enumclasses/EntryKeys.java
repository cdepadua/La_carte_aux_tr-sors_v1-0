package com.cesaredepadua.treasuremap.job.enumclasses;

/**
 * @author cesar
 * Enum of key
 */
public enum EntryKeys {
	/**
	 * Map size
	 */
	MAP_SIZE("C*", new String[] {"key", "width","height"}),
	/**
	 * Mountain
	 */
	MOUNTAIN("M*", new String[] {"key","xPos","yPos"}),
	/**
	 * Treasure
	 */
	TRASURE("T*", new String[] {"key","xPos","yPos","nbOfTreasures"}),
	/**
	 * Adventurer
	 */
	ADVENTURER("A*", new String[] {"key","name", "xPos","yPos","orientation","movements"});
	
	/** Key in file. */
	private final String key;
	/** Values. */
	private final String[] valuesHeaders;
	
	/** Constructor. 
	 * @param key key
	 * @param values list ov values*/
	EntryKeys(String key, String[] values)
	{
		this.key = key;
		this.valuesHeaders = values;
	}
	
	/**
	 * @return key
	 */
	public String key()
	{
		return this.key;
	}
	/**
	 * @return headers
	 */
	public String[] valuesHeaders()
	{
		return this.valuesHeaders;
	}
	
}

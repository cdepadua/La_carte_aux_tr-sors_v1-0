package com.cesaredepadua.treasuremap.job.enumclasses;

/**
 * @author cesar
 * Enum of movements
 */
public enum Move {
	/**
	 * Advance
	 */
	ADVANCE("A"),
	/**
	 * Turn to right
	 */
	RIGHT("D"),
	/**
	 * Turn to left
	 */
	LEFT("G");
	
	/**
	 * symbol
	 */
	private String symbol;
	
	/**
	 * @param s symbol
	 */
	Move(String s)
	{
		this.symbol = s;
	}
	
	/**
	 * @return symbol
	 */
	public String symbol()
	{
		return this.symbol;
	}
	
	/**
	 * @param m move char
	 * @return mov value
	 */
	public static Move move(char m)
	{
		switch(m)
		{
			case 'A': return Move.ADVANCE;
			case 'D': return Move.RIGHT;
			case 'G': return Move.LEFT;
			default:  return null;
		}
	}
	
}

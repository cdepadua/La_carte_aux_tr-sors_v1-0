package com.cesaredepadua.treasuremap.job.buisinessclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.impl.Adventurer;
import com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.impl.Field;
import com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.impl.Treasure;
import com.cesaredepadua.treasuremap.job.enumclasses.TilesType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author cesar
 * Simulation business class.
 */
@Component
public class Simulation {
	/** Field separator. */
	@Value("${batch.output.file.separator}")
	private String separator;
	/** Map. */
	@Autowired @Getter @Setter
	private SimulationMap map;
	/** Adventurers. */
	@Getter @Setter
	private List<Adventurer> adventurers;
	
	/**
	 * Private method to print size map
	 * @return size map to string
	 */
	private String returnLineSizeMap()
	{
		return new StringBuilder().append(TilesType.MAP_SIZE.symbol()).append(separator)
		.append(this.map.getWidth()).append(separator)
		.append(this.map.getHeight()).toString();
	}
	
	/**
	 * Private method to print mountain informations
	 * @param field mountain tile
	 * @return mountain to string
	 */
	private String returnLineMountain(Field field)
	{
		return new StringBuilder().append(TilesType.MOUNTAIN.symbol())
		.append(separator).append(field.getX()).append(separator)
		.append(field.getY()).toString();
	}
	
	/**
	 * Private method to print treasure informations
	 * @param treasure treasure tile
	 * @return treasure to string
	 */
	private String returnLineTreasure(Treasure treasure)
	{
		return new StringBuilder().append(TilesType.TREASURE.symbol())
		.append(separator).append(treasure.getX()).append(separator)
		.append(treasure.getY()).append(separator).append(treasure.getNbOftreasures())
		.toString();
	}
	
	/**
	 * Private method to print adventurer informations
	 * @param adv adventurer tile
	 * @return adventurer to string
	 */
	private String returnLineAdventurer(Adventurer adv)
	{
		return new StringBuilder().append(TilesType.ADVENTURER.symbol()).append(separator)
		.append(adv.getName()).append(separator).append(adv.getX()).append(separator)
		.append(adv.getY()).append(separator).append(adv.getOrientation().symbol())
		.append(separator).append(adv.getNbOfTreasures()).toString();
	}
	
	/**
	 * Private method to prepare the list of adventurers to be printed
	 * @return list of adventurers to string
	 */
	private List<String> prepareAdventurersToBePrinted()
	{
		List<String> advs = new ArrayList<>();
		this.adventurers.forEach(a -> {
			advs.add(this.returnLineAdventurer(a));
		});
		return advs;
	}

	/**
	 * Return the simulation to string
	 * @return simulation to string
	 */
	public List<String> returnPrintableSimulation()
	{
		List<String> listToReturn = new ArrayList<>();
		List<String> treasures = new ArrayList<>();
		listToReturn.add(this.returnLineSizeMap());
		for(Integer i = 0; i < map.getHeight(); i++)
		{
			for (Integer j = 0; j < map.getWidth(); j++)
			{
				Field field = this.map.getTiles()[i][j];
				if(field.getType().equals(TilesType.MOUNTAIN))
					listToReturn.add(this.returnLineMountain(field));
				else if(field.getType().equals(TilesType.TREASURE) && ((Treasure)field).getNbOftreasures() > 0)
					treasures.add(this.returnLineTreasure((Treasure) field));
			}
		}
		listToReturn = Stream.concat(listToReturn.stream(), treasures.stream())
				.collect(Collectors.toList());
		
		return Stream.concat(listToReturn.stream(), this.prepareAdventurersToBePrinted().stream())
				.collect(Collectors.toList());
	}
}

package com.cesaredepadua.treasuremap.job.steps.simulationstep.processor;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.cesaredepadua.treasuremap.job.buisinessclasses.Simulation;
import com.cesaredepadua.treasuremap.job.services.SimulationService;

/**
 * @author cesar
 *
 */
public class SimulationStepProcessor implements ItemProcessor<List<Map<String, String>>,List<String>>{
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SimulationStepProcessor.class);
	
	/**
	 * Simulation service
	 */
	@Autowired
	/** Simulation service. */
	private SimulationService simulationService;
	
	@Override
	public List<String> process(final List<Map<String, String>> values) throws Exception
	{
		LOG.info(values.toString());
		Simulation simulation= this.simulationService.initSimulation(values);
		this.simulationService.runSimulation(simulation);
		return simulation.returnPrintableSimulation();
	}
}

package com.cesaredepadua.treasuremap.job.buisinessclasses.tiles.impl;

import com.cesaredepadua.treasuremap.job.enumclasses.TilesType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author cesar
 * Treasure business class
 */
public class Treasure extends Field{
	
	/** Number of Tresures. */
	@Getter @Setter
	private Integer nbOftreasures;
	/** Constructor. 
	 * @param t tile type*/
	public Treasure(TilesType t)
	{
		super(t);
	}
	@Override
	public String toString()
	{
		return new StringBuilder().append(this.type.symbol()).append("(")
				.append(this.nbOftreasures.toString()).append(")").toString();
	}
	
}
